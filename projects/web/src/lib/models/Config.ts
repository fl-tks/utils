export interface Config {
    projectKey: NonNullable<string>;
    adminAuth: NonNullable<string>;
    apiUrl: NonNullable<string>;
    resourcesAuth: NonNullable<string>;
    resourcesUrl: NonNullable<string>;
    defaultLanguage: NonNullable<string>;
    enableBenchmark: boolean;
}
