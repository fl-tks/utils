export interface UploadResponse {
  Name: NonNullable<string>;
  Size: NonNullable<number>;
  Url: NonNullable<string>;
  Active: boolean;
}