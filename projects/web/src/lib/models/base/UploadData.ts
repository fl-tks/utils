export interface UploadData {
  resource: NonNullable<File>;
  name?: string;
  size?: number;
}