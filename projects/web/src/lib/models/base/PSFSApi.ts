export interface PSFSApi {
    __pk: NonNullable<number>;
    __name__?: NonNullable<string>;
}