/**
 * PSFS standard JSON Response
 */
export class ApiResponse<T> {
    success: boolean;
    total?: NonNullable<number>;
    pages?: NonNullable<number>;
    message?: string;
    data: T;
}
