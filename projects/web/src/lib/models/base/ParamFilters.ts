export class ParamFilters {
    limit= 10;
    page: number;
    fields: Object;

    constructor(fields = {}, page: number, limit: number = 10) {
        this.fields = fields
        this.page = page;
        this.limit = limit;
    }
}