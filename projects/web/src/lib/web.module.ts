import {APP_INITIALIZER, ModuleWithProviders, NgModule} from '@angular/core';
import {WebComponent} from './web.component';
import {CommonModule} from "@angular/common";
import {ApiService} from "./services/api.service";
import {BenchmarkComponent} from "./benchmark.component";
import {Config} from "./models/Config";
import {WebConfigService} from "./services/web.config";
import {throwError} from "rxjs";


@NgModule({
    declarations: [WebComponent],
    imports: [
        CommonModule
    ],
    exports: [WebComponent],
    providers: [ApiService, BenchmarkComponent]
})
export class WebModule {
    static config(data: Config): ModuleWithProviders {
        return {
            ngModule: WebModule,
            providers: [
                ApiService,
                {
                    provide: APP_INITIALIZER,
                    multi: true,
                    useFactory: (webConfigService: WebConfigService) => {
                        webConfigService.setWebUtilsConfig(data)
                            .catch((error) => {
                              throwError(error);
                            });
                    },
                    deps: [WebConfigService]
                }
            ]
        };
    }
}
