import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {

  constructor() { }
  
  static padDigits(number, digits, pad: any = '0') {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(pad) + number;
  }
}
