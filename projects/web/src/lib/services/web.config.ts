import {Injectable} from "@angular/core";
import {Config} from "../models/Config";

@Injectable()
export class WebConfigService {
    static data = {
        admin: '',
        customerCode: 'localhost',
        apiUrl: '',
        enableBenchmark: true,
        defaultLanguage: 'es',
        resourcesUrl: '',
        resourcesUser: ''
    };
    setWebUtilsConfig(moduleConfig: Config) {
        return new Promise((resolve, reject) => {
            /**
             * OPTIONAL VARIABLES
             */
            if(moduleConfig.projectKey) {
                WebConfigService.data.customerCode = moduleConfig.projectKey;
            }
            if(moduleConfig.defaultLanguage) {
                WebConfigService.data.defaultLanguage = moduleConfig.defaultLanguage;
            }
            if(null !== moduleConfig.enableBenchmark) {
                WebConfigService.data.enableBenchmark = moduleConfig.enableBenchmark;
            }
            if(null !== moduleConfig.resourcesAuth) {
                WebConfigService.data.resourcesUser = moduleConfig.resourcesAuth;
            }
            if(null !== moduleConfig.resourcesUrl) {
                WebConfigService.data.resourcesUrl = moduleConfig.resourcesUrl;
            }
            /**
             * REQUIRED VARIABLES
             */
            if(moduleConfig.adminAuth) {
                WebConfigService.data.admin = moduleConfig.adminAuth;
            } else {
                reject('Var admin not defined in environment file');
            }
            if(moduleConfig.apiUrl) {
                WebConfigService.data.apiUrl = moduleConfig.apiUrl;
            } else {
                reject('Var apiUrl not defined in environment file');
            }
            resolve();
        });
    }
}
