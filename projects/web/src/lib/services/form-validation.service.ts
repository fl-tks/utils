import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class FormValidationService {
    phoneNumberPattern = "^((\\+91-?)|0)?[0-9]{9}$";
    postCodePattern = "^([0-9]{5})([\-]{1}[0-9]{4})?$";

    constructor() {
    }
    
    public static matchValues(
        matchTo: string // name of the control to match to
    ): (AbstractControl) => ValidationErrors | null {
        return (control: AbstractControl): ValidationErrors | null => {
            const isMatching = !!control.parent &&
                !!control.parent.value &&
                control.value === control.parent.controls[matchTo].value;
            return isMatching
                ? null
                : { isMatching: false };
        };
    }
    
    validateFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateFormFields(control);
            }
        });
    }
    
    fieldHasErrors(field: string, form: FormGroup) {
        const fieldFromForm = form.get(field);
        const errors = fieldFromForm.errors;
        let hasError = false;
        if (errors) {
            Object.keys(errors).forEach((key, error) => {
                if ('pattern' !== key && !fieldFromForm.valid) {
                    hasError = true;
                }
            });
        }

        switch (field) {
            case 'confirmPassword':
                hasError = form.hasError('isMatching');
                break;
        }
        return hasError && fieldFromForm.touched;
    }

    fieldWithInvalidPattern(field: string, form: FormGroup) {
        const fieldFromForm = form.get(field);
        const errors = fieldFromForm.errors;
        let errorPattern = false;
        if (errors) {
            Object.keys(errors).forEach((key, error) => {
                if ('pattern' === key && !fieldFromForm.valid) {
                    errorPattern = true;
                }
            });
        }
        return errorPattern && fieldFromForm.touched;
    }
    
    displayFieldCss(field: string, form) {
        return {
            'is-invalid': this.fieldHasErrors(field, form) || this.fieldWithInvalidPattern(field, form)
        };
    }

    getPhonePattern() {
        return this.phoneNumberPattern;
    }

    getPostCodePattern() {
        return this.postCodePattern;
    }
}
