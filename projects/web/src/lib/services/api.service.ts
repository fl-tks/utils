import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, map, retry, tap} from 'rxjs/operators';
import {BehaviorSubject, from, Observable, throwError} from 'rxjs';
import {BenchmarkComponent} from "../benchmark.component";
import {ApiResponse} from "../models/base/ApiResponse";
import {UploadResponse} from "../models/base/UploadResponse";
import {WebConfigService} from "./web.config";

@Injectable({
  providedIn: 'root'
})
export class ApiService extends BenchmarkComponent {
  static USER_LANGUAGE_VARIABLE = '__user_lang__';
  static CUSTOMER_TOKEN_KEY = '__mtct__';
  static USER_TOKEN_KEY = '__ut__';
  private REST_API_URL = WebConfigService.data.apiUrl;
  private REST_SRV_URL = WebConfigService.data.resourcesUrl;
  private CUSTOMER_TOKEN: NonNullable<string>;
  private API_LANG = WebConfigService.data.defaultLanguage;
  private USER_TOKEN: NonNullable<string>;

  changeLanguageSubject = new BehaviorSubject<string>(WebConfigService.data.defaultLanguage);
  customerTokenSubject = new BehaviorSubject<boolean>(false);
  customerTokenLoaded: Observable<boolean> = this.customerTokenSubject.asObservable();

  constructor(private httpClient: HttpClient) {
    super();
    this.USER_TOKEN = localStorage.getItem(ApiService.USER_TOKEN_KEY);
    this.CUSTOMER_TOKEN = localStorage.getItem(ApiService.CUSTOMER_TOKEN_KEY);
    this.API_LANG = localStorage.getItem(ApiService.USER_LANGUAGE_VARIABLE);
  }

  onChangeLanguage() {
    return this.changeLanguageSubject.asObservable();
  }

  sync() {
    return this.customerTokenLoaded;
  }

  /**
   * Service that gather the customer token
   */
  private __sync;

  private endSyncMeasure() {
    this.benchmark('syncCustomerTokenEnd');
    this.customerTokenSubject.next(true);
    this.measure('syncCustomerTokenStart', 'syncCustomerTokenEnd');
  }

  syncCustomerToken() {
    clearTimeout(this.__sync);
    this.__sync = setTimeout(() => {
      this.benchmark('syncCustomerTokenStart');
      if (!this.CUSTOMER_TOKEN || this.CUSTOMER_TOKEN.length === 0) {
        this.httpClient.post(this.REST_API_URL + '/admin/sync/' + WebConfigService.data.customerCode, null, {
          headers: new HttpHeaders({
            Authorization: 'Basic ' + WebConfigService.data.admin,
            'X-API-LANG': this.API_LANG
          })
        }).pipe(
            retry(3),
            catchError(this.handleError)
        ).subscribe(
            (customerToken: ApiResponse<{ token: string }>) => {
              if (customerToken.success) {
                this.CUSTOMER_TOKEN = customerToken.data.token;
                localStorage.setItem(ApiService.CUSTOMER_TOKEN_KEY, this.CUSTOMER_TOKEN);
                this.endSyncMeasure();
              }
            }
        );
      } else {
        this.endSyncMeasure();
      }
    }, 100);
  }

  public setLanguage(language: NonNullable<string>) {
    this.API_LANG = language;
    this.changeLanguageSubject.next(this.API_LANG);
  }

  public setUserToken(userToken: NonNullable<string>) {
    this.USER_TOKEN = userToken;
    localStorage.setItem(ApiService.USER_TOKEN_KEY, userToken);
    this.benchmark('User token set');
  }

  public getUserToken() {
    return this.USER_TOKEN;
  }

  private generateHeaders(needsAuthCustomer?: boolean, isBasicAuth?: boolean) {
    const _headers = {
      'X-API-LANG': this.API_LANG,
      'X-CLIENT-TOKEN': this.CUSTOMER_TOKEN || ''
    };
    if (this.USER_TOKEN && !isBasicAuth) {
      _headers['Authorization'] = 'Bearer ' + this.getUserToken();
    }
    if (needsAuthCustomer) {
      _headers['X-AUTH-CUSTOMER'] = WebConfigService.data.customerCode;
    }
    if (isBasicAuth) {
      _headers['Authorization'] = 'Basic ' + WebConfigService.data.admin;
    }
    return new HttpHeaders(_headers);
  }

  private handleError(error: HttpErrorResponse) {
    // TODO something
    return throwError(error);
  }

  public $get(url: string, query?: HttpParams, addCustomerCode?: boolean, isBasicAuth?: boolean): Observable<any> {
    this.benchmark('[GET] ' + url + ' Requested');
    return this.httpClient.get(this.REST_API_URL + url, {
      headers: this.generateHeaders(addCustomerCode, isBasicAuth),
      params: query
    }).pipe(
        tap(() => {
          this.benchmark('[GET] ' + url + ' Finished');
          this.measure('[GET] ' + url + ' Requested', '[GET] ' + url + ' Finished');
        }),
        catchError(this.handleError)
    );
  }

  public $post(url: string, data?: any, addCustomerCode?: boolean, isBasicAuth?: boolean): Observable<any> {
    this.benchmark('[POST] ' + url + ' Requested');
    return this.httpClient.post(this.REST_API_URL + url, data, {
      headers: this.generateHeaders(addCustomerCode, isBasicAuth)
    }).pipe(
        tap(() => {
          this.benchmark('[POST] ' + url + ' Finished');
          this.measure('[POST] ' + url + ' Requested', '[POST] ' + url + ' Finished');
        }),
        catchError(this.handleError)
    );
  }

  public $put(url: string, data?: any, addCustomerCode?: boolean, isBasicAuth?: boolean): Observable<any> {
    this.benchmark('[PUT] ' + url + ' Requested');
    return this.httpClient.put(this.REST_API_URL + url, data, {
      headers: this.generateHeaders(addCustomerCode, isBasicAuth)
    }).pipe(
        tap(() => {
          this.benchmark('[PUT] ' + url + ' Finished');
          this.measure('[PUT] ' + url + ' Requested', '[PUT] ' + url + ' Finished');
        }),
        catchError(this.handleError)
    );
  }

  public $delete(url: string, query?: HttpParams, addCustomerCode?: boolean, isBasicAuth?: boolean): Observable<any> {
    this.benchmark('[DELETE] ' + url + ' Requested');
    return this.httpClient.delete(this.REST_API_URL + url, {
      headers: this.generateHeaders(addCustomerCode, isBasicAuth),
      params: query
    }).pipe(
        tap(() => {
          this.benchmark('[DELETE] ' + url + ' Finished');
          this.measure('[DELETE] ' + url + ' Requested', '[DELETE] ' + url + ' Finished');
        }),
        catchError(this.handleError)
    );
  }

  public $list(url: string, filters?: HttpParams, page: number = 1, limit: number = 25) {
    const _filter = {};
    if (filters) {
      const keys = filters.keys();
      for (const loopKey in keys) {
        if (keys.hasOwnProperty(loopKey)) {
          const key = keys[loopKey];
          _filter[key] = filters.get(key);
        }
      }
    }
    const apiQuery = new HttpParams({
      fromObject: _filter
    }).set('__page', page.toString())
        .set('__limit', limit.toString());
    this.benchmark('[LIST] ' + url + ' Requested');
    return this.httpClient.get(this.REST_API_URL + url, {
      headers: this.generateHeaders(),
      params: apiQuery
    }).pipe(
        tap(() => {
          this.benchmark('[LIST] ' + url + ' Finished');
          this.measure('[LIST] ' + url + ' Requested', '[LIST] ' + url + ' Finished');
        }),
        catchError(this.handleError));
  }

  public $upload(data: FormData): Promise<UploadResponse> {
    this.benchmark('[UPLOAD] Requested');
    return this.httpClient
        .post<ApiResponse<UploadResponse>>(this.REST_SRV_URL, data, {
          headers: new HttpHeaders({
            Authorization: 'Basic ' + WebConfigService.data.resourcesUser,
            'X-API-LANG': this.API_LANG
          })
        }).pipe(
            tap(() => {
              this.benchmark('[UPLOAD] Finished');
              this.measure('[UPLOAD] Requested', '[UPLOAD] Finished');
            }),
            catchError(this.handleError),
            map((response: ApiResponse<UploadResponse>) => response.data)
        ).toPromise();
  }

  public $handshake() {
    if (this.USER_TOKEN) {
      this.benchmark('[HANDSHAKE] Requested');
      const url = '/AUTH/Api/handshake/' + this.getUserToken();
      return this.$post(url, null, true, false)
          .pipe(
              tap((response: ApiResponse<{ sessionToken: string }>) => {
                if (!response.success) {
                  this.USER_TOKEN = null;
                  localStorage.removeItem(ApiService.USER_TOKEN_KEY);
                }
                this.benchmark('[HANDSHAKE] Finished');
                this.measure('[HANDSHAKE] Requested', '[HANDSHAKE] Finished');
              }),
              catchError(this.handleError)
          );
    } else {
      const response = new ApiResponse();
      response.success = true;
      response.data = false;
      return from([response]);
    }
  }

  public $logout() {
    return this.httpClient.delete(this.REST_API_URL + '/AUTH/Api/logout/' + this.getUserToken(), {
      headers: this.generateHeaders(true)
    }).pipe(
        tap(() => {
          this.USER_TOKEN = null;
          const ct = localStorage.getItem(ApiService.CUSTOMER_TOKEN_KEY);
          localStorage.clear();
          localStorage.setItem(ApiService.CUSTOMER_TOKEN_KEY, ct);
        }),
        catchError(this.handleError)
    );
  }
}
