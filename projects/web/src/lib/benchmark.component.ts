import {WebConfigService} from "./services/web.config";
/**
 * Class to benchmark all traces in the project
 */

export class BenchmarkComponent {
  private projectKey = WebConfigService.data.customerCode;

  constructor() {
    this.benchmark('Instance created');
  }

  protected benchmark(message: NonNullable<string>) {
    const log = '[' + this.projectKey + '][' + this.constructor.name + '] ' + message;
    if (WebConfigService.data.enableBenchmark) {
      performance.mark(log);
    }
    console.debug((new Date()).getTime() + ': ' + log);
  }

  protected measure(mark1: NonNullable<string>, mark2?: NonNullable<string>) {
    const log1 = '[' + this.projectKey + '][' + this.constructor.name + '] ' + mark1;
    const log2 = '[' + this.projectKey + '][' + this.constructor.name + '] ' + mark2;

    if (performance.getEntriesByName(log1).length && WebConfigService.data.enableBenchmark) {
      performance.measure('Measure -->', log1, log2);
    }
  }
}
